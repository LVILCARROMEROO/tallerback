const db = [
    { 
        slug: 'arquitectura-de-angular-7',
        title: 'Arquitectura de Angular 7',
        brief: 'Es este taller aprenderemos los conceptos y uso de componentes, directivas, rutas y otras…',
        description: 'Es este taller aprenderemos los conceptos y uso de componentes, directivas, rutas y otras características que nos ofrece Angular 7.',
        poster: 'https://galaxy.edu.pe/wp-content/uploads/2019/05/icono-taller-angular.jpg',
        date: new Date('2019-05-28'),
        start: new Date('2019-05-28 19:30'),
        end: new Date('2019-05-28 21:00'),
        vacancies: 'Limitado'
    },
    { 
        slug: 'despliegue-de-aplicaciones-mvc-net-core-en-linuxaws',
        title: 'Despliegue de Aplicaciones MVC NET Core en Linux(AWS)',
        brief: 'Caso práctico de creación de Aplicaciones MVC NET Core y despliegue en Linux(CentOS)…',
        description: 'Caso práctico de creación de Aplicaciones MVC NET Core y despliegue en Linux(CentOS) hospedado en Amazon Web Services (AWS)',
        poster: 'https://galaxy.edu.pe/wp-content/uploads/2019/05/23052109icono.png',
        date: new Date('2019-05-23'),
        start: new Date('2019-05-23 19:30'),
        end: new Date('2019-05-23 21:00'),
        vacancies: 'Limitado'
    },
    { 
        slug: 'automatizacion-de-despliegue-de-aplicaciones-con-docker-y-kubernetes',
        title: 'Automatización de despliegue de aplicaciones con Docker y Kubernetes',
        brief: 'Aprende de manera práctica y dinámica a automatizar el despliegue de aplicaciones dockerizadas usando Kubernetes; con el objetivo de minimizar esfuerzos…',
        description: 'Aprende de manera práctica y dinámica a automatizar el despliegue de aplicaciones dockerizadas usando Kubernetes; con el objetivo de minimizar esfuerzos y optimizar recursos de infraestructura tecnológica.',
        poster: 'https://galaxy.edu.pe/wp-content/uploads/2019/05/21052019.jpg',
        date: new Date('2019-05-21'),
        start: new Date('2019-05-21 19:30'),
        end: new Date('2019-05-21 21:00'),
        vacancies: 'Limitado'
    },
    { 
        slug: 'aplicaciones-web-con-asp-net-core-mvc-6-0',
        title: 'Aplicaciones Web con ASP.NET Core MVC 6.0',
        brief: 'Implementación de operaciones CRUD con Entity Framework Core (EF Core) y ASP.NET Core 6.0 MVC. Se realizará un introducción teórica y el desarrollo de un caso práctico.',
        description: 'Implementación de operaciones CRUD con Entity Framework Core (EF Core) y ASP.NET Core 6.0 MVC. Se realizará un introducción teórica y el desarrollo de un caso práctico.',
        poster: 'https://galaxy.edu.pe/wp-content/uploads/2019/05/14052019icono.png',
        date: new Date('2019-05-14'),
        start: new Date('2019-05-14 19:30'),
        end: new Date('2019-05-14 21:00'),
        vacancies: 'Limitado'
    },
    { 
        slug: 'integracion-de-jpa-hibernate-spring-y-primefaces',
        title: 'Integración de JPA, Hibernate, Spring y PrimeFaces',
        brief: 'Aprende a integrar y maximizar el uso de las ventajas que ofrece Hibernate, Spring y PrimeFaces para el desarrollo de aplicaciones web..',
        description: 'Aprende a integrar y maximizar el uso de las ventajas que ofrece Hibernate, Spring y PrimeFaces para el desarrollo de aplicaciones web. Incluye el desarrollo un caso práctico y recomendaciones para su integración.',
        poster: 'https://galaxy.edu.pe/wp-content/uploads/2019/05/09052019icono-taller_.jpg',
        date: new Date('2019-05-09'),
        start: new Date('2019-05-09 19:30'),
        end: new Date('2019-05-09 21:00'),
        vacancies: 'Limitado'
    },
    { 
        slug: 'servicios-restful-con-spring-boot',
        title: 'Servicios RESTful con Spring Boot',
        brief: 'Aprende a utilizar Spring Boot para desarrollar Servicios RESTful utilizando Spring Data y Oracle a través de un caso práctico. Incluye',
        description: 'Aprende a utilizar Spring Boot para desarrollar Servicios RESTful utilizando Spring Data y Oracle a través de un caso práctico. Incluye configuración y pruebas en Postman y JSONLint.',
        poster: 'https://galaxy.edu.pe/wp-content/uploads/2019/05/07052019icono-taller.jpg',
        date: new Date('2019-05-07'),
        start: new Date('2019-05-07 19:30'),
        end: new Date('2019-05-07 21:00'),
        vacancies: 'Limitado'
    },
    { 
        slug: 'gestione-escale-y-despliega-contenedores-de-forma-automatiza-con-kubernetes',
        title: 'Gestione, escale y despliega contenedores de forma automatizada con Kubernetes',
        brief: 'Con Kubernetes podrás automatizar el despliegue, escalamiento y gestión de tus aplicaciones Docker con poco esfuerzo y poco recursos de Hardware. En este taller se mostrará las herramientas más conocidas',
        description: 'Aprende a utilizar Spring Boot para desarrollar Servicios RESTful utilizando Spring Data y Oracle a través de un caso práctico. Incluye configuración y pruebas en Postman y JSONLint.',
        poster: 'https://galaxy.edu.pe/wp-content/uploads/2019/04/icono-taller-gratuito_26.jpg',
        date: new Date('2019-05-02'),
        start: new Date('2019-05-02 19:30'),
        end: new Date('2019-05-02 21:00'),
        vacancies: 'Limitado'
    },
]

module.exports = db;